# Schulferien für Ubuntu Touch

Shows school vacations for German states.

Features:
- Mark currently active vacations
- Export vacation list as ics file for importing in other calendars
- Saves last state selection and restores it when the app starts
- Dark mode

This app consumes data from the <a href="https://ferien-api.de/">Ferien-API</a> by Paul Brejla. No guarantee is given for the correctness and completeness of the data. Please see credits in the app ("questionmark" button) for details.

-----------------------------------------------------------

Zeigt Schulferientermine für deutsche Bundesländer.

Features:
- Kennzeichnet momentan aktive Ferienzeit
- Export der Ferientermine als ics-Datei für den Import in andere Kalender
- Speichert die letzte Auswahl des Bindeslandes und stellt sie bei jedem Start der App wieder her
- Dark Mode

Diese App bezieht Daten aus der <a href="https://ferien-api.de/">Ferien-API</a> von Paul Brejla. Für die Richtigkeit und Vollständigkeit der Daten wird keine Garantie übernommen. Bitte lesen Sie auch die Infos in der App ("Fragezeichen"-Button) für weitere Details.

## License

Copyright (C) 2022 - 2023 Daniel Schlieckmann

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
